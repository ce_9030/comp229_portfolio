let mongoose = require('mongoose');

// create a model class
let entryModel = mongoose.Schema({
    fullName: String,
    email: String,
    phone: String
},
{
    collection: "entries"
});

module.exports = mongoose.model('Entry', entryModel);