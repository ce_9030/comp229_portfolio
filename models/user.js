//Dependencies
let mongoose = require('mongoose');
let passportLocalMongoose = require('passport-local-mongoose');

let User = mongoose.Schema (
{
    username: {
        type: String,
        default: "",
        trim: true,
        required: "username is required"
    },
		
		password: {
        type: String,
        default: "",
        trim: true,
        required: "You can't proceed without a password."
    }
}, {
	collection: "users"
});

//Config
let options = ({missingPasswordError: 'Either there is no password, or the password entered is invalid.'});
User.plugin(passportLocalMongoose, options);

module.exports.User = mongoose.model('User',User);