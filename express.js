/* express.js - Cameron Edwards - 301129134 - 10/01/21 to 10/20/21*/
///Initialize packages
let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

//authmodules
let session=require('express-session');
let passport = require('passport');
let passportLocal = require('passport-local');
let localStrategy = passportLocal.Strategy;
let flash = require('connect-flash');

// database setup
let mongoose = require('mongoose');
let DB = require('./db');

//point mongoose to the db uri
mongoose.connect(DB.URI, {useNewUrlParser: true, useUnifiedTopology: true});

let mongoDB = mongoose.connection;

mongoDB.on('error', console.error.bind(console, 'Connection Error:'));

mongoDB.once('open', () => {
  console.log('Connected to MongoDB...');
});

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
//let contactRouter = require('./routes/contact');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); //express -e

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '../../node_modules')));

//Setup express session
app.use(session({
	secret: "Something",
	saveUninitialized: false,
	resave: false
}));

// initialize flash
app.use(flash());

// initialize passport
app.use(passport.initialize());
app.use(passport.session());

// passport user configuration

// create a User model instance
let userModel = require('./models/user');
let User = userModel.User;

// implemente a User Authentication Strategy
passport.use(User.createStrategy());

// serialize and deserialize the User Info
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser()); 

///Set up routes
app.use('/', indexRouter);
app.use('/users', usersRouter);
//app.use('/contact', contactRouter);

module.exports = app;
