/* index.js - Cameron Edwards - 301129134 - 10/01/21*/
var express = require('express');
var router = express.Router();

// Connect to controller, allowing for database functions
let dbController = require('../dbcontroller');
let authController = require('../authcontroller');

/*this page will be the first to open.*/
router.get('/', function(req,res,next) {
res.render('index', {
title: 'INDEX // 5.153khz',
header: 'Hello and welcome!'});
});

/*GET frontpage.*/
router.get('/index', function(req,res,next) {
res.render('index', {
title: 'INDEX // 5.153khz',
header: 'Hello and welcome!'});
});

/* GET about page. */
router.get('/about', function(req, res, next) {
res.render('about', {
title: 'ABOUT // 5.153khz',
header: 'Who am I?'});
});

/* GET projects page. */
router.get('/projects', function(req, res, next) {
res.render('projects', {
title: 'PROJECTS // 5.153khz',
header: 'What I have done so far.'});
});

/* GET services page. */
router.get('/services', function(req, res, next) {
res.render('services', {
title: 'SERVICES // 5.153khz',
header: 'What I can do for others.'});
});

/* GET contact page. */
router.get('/contact', function(req, res, next) {
res.render('contact', {
title: 'CONTACT // 5.153khz',
header: 'Need to reach me?'});
});
/* GET login failed page. */
router.get('/loginfailed', function(req, res, next) {
res.render('loginfailed', {
title: 'ERROR // 5.153khz',
header: 'Login Error'});
});

/* GET login page. */
router.get('/login', authController.displayLoginPage);
router.post('/login', authController.processLoginPage);

/* GET registration page. */
router.get('/register', authController.displayRegisterPage);
router.post('/register', authController.processRegisterPage);

/* Process logout operation. */
router.get('/logout/',authController.performLogout);

/* GET listings page. */
router.get('/listings', dbController.displayEntryList);

/* GET add page. */
router.get('/listingsadd', dbController.displayAddPage);
router.post('/listingsadd', dbController.processAddPage);

/* GET edit page. */
router.get('/listingsupdate/:id',dbController.displayEditPage);
router.post('/listingsupdate/:id',dbController.processEditPage);

/* Process delete operation. */
router.get('/delete/:id',dbController.removeEntry);

module.exports = router;