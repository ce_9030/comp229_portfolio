// connect to entry model
let Entry = require('./models/entry');

module.exports.displayEntryList = (req, res, next) => {
    Entry.find((err, entryList) => {
        if (err) {
            return console.error(err);
        } else {
		if (req.user)
            res.render('listings', {title: 'LISTINGS // 5.153khz',header: 'Contact Listings', EntryList : entryList});
			else res.redirect('login');
        }
    });
};

module.exports.displayAddPage = (req, res, next) => {
if (req.user)
    res.render('listingsadd', {title: 'ENTRY CREATION // 5.153khz', header: 'New Entry'});
	else res.redirect('login');
}

module.exports.processAddPage = (req, res, next) => {
    let newEntry = Entry({
        "fullName": req.body.fullName,
		"phone": req.body.phone,
		"email": req.body.email
    });

    Entry.create(newEntry, (err, Entry) => {
        if (err) {
            console.log(err);
            res.end(err);
        } else {
            // refresh the entry list
            res.redirect('listings');
        }
    });
}

module.exports.displayEditPage =  (req, res, next) => {
    let id = req.params.id;

    Entry.findById(id, (err, entryToEdit) => {
        if (err) {
            console.log(err);
            res.end(err);    
        } else if (req.user) {
            //show the edit view
            res.render('listingsupdate', {title: 'UPDATE ENTRY // 5.153khz',header: 'Rewriting Entry', entry: entryToEdit})
        } else res.redirect('../login');
    })
}

module.exports.processEditPage =  (req, res, next) => {
    let id = req.params.id;

    let updatedEntry = Entry({
        "_id": id,
        "fullName": req.body.fullName,
        "email": req.body.email,
		"phone": req.body.phone
        });
    
    Entry.updateOne({_id: id}, updatedEntry, (err) => {
        if (err) {
            console.log(err);
            res.end(err);    
        } else {
            // refresh the entry list
            res.redirect('../listings');
        }
    });

}

module.exports.removeEntry =  (req, res, next) => {
    let id = req.params.id;
if (req.user) {
    Entry.deleteOne({_id: id}, (err) => {
        if (err) {
            console.log(err);
            res.end(err);    
        } else {
            // refresh the entry list
            res.redirect('../listings');
        }
    });} else res.redirect('../login');
}