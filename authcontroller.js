let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');
let passport = require('passport');

//create user model instance
let userModel = require('./models/user');
let User = userModel.User; // alias

module.exports.displayLoginPage = (req, res, next) => {
    // check if user is not already loged in
    if (!req.user) {
        res.render('login', {
            title: "Login",
            header: "Barricade",
			displayName: req.user ? req.user.displayName : ''
        })
    }
    else {
        res.redirect('/listings');
    }
}

module.exports.processLoginPage = (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        //if server err?
        if (err) {
            return next(err);
        }

        //is there a user login error?
        if (!user) {
            header: "Auth Error Encountered"
            //return res.redirect('login');
        } 

        req.login(user, (err) => {
            // server error?
            if(err) {
				return res.redirect('/loginfailed');
                //return next(err);
            }
            return res.redirect('/listings');
        });
        
    })(req, res, next);
}

module.exports.displayRegisterPage = (req, res, next) => {
    // check if user is not already loged in
    if (!req.user) {
        res.render('register', {
            title: "REGISTRATION // 5.153khz",
            header: "Account Creation"
        });
    }
    else {
        res.redirect('/');
    }
}

module.exports.processRegisterPage = (req, res, next) => {
    // instantiate a user object
    let newUser = new User({
        username: req.body.username,
        password: req.body.password
    })

    User.register(newUser, req.body.password, (err) => {
        if(err) {
            console.log(err);
            if(err.name == "UserExistsError") {
                req.flash('registerMessage', 'Registration Error: User Already Exists!');
                console.log('Error: User Already Exists!')
            }
            return res.render('register', {
            title: "REGISTRATION // 5.153khz",
            header: "Account Creation",
                displayName: req.user ? req.user.displayName : ''
            });
        } else {
            // if no error exists, then  registration is successful

            // redirect the user and authenticate

            return passport.authenticate('local')(req, res, () => {
                res.redirect('/listings');
            })
        }

    })
}

module.exports.performLogout = (req, res, next) => {
    req.logout();
    res.redirect('/');
}
